---
title: Our Mathematical Universe
category: non-fiction / science
score: 4
---

A great book I have enjoyed a lot. For the first time I have read
something related to physics/mathematics that attempts to explain
our reality as a whole. It's also good for an introduction to the
most recent physics theories and learning from a great MIT
professor.
