---
title: Books
date: 2017-03-23
publishdate: 2017-03-24
---

These are some of the books I have read. I'll try to be as rigorous as
I can. A few notes about this lists:

* I'm not trying to make a thorough description of each book. My
  intention is to try to keep a log just for revisiting it in the
  future.

* I'll use the language on which the book was written to describe
  it.
