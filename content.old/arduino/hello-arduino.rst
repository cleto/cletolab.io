.. -*- mode:rst; coding:utf-8 -*-

Hello Arduino!
##############

:tags: arduino
:category: reviews
:slug: hello-arduino
:author: cleto
:summary: Recently I have started to learn Arduino in order to improve
   my skills on *low-level* programming and, at least once in my
   lifetime, understand some basic electronic concepts. However, I've
   never liked the idea of using something different to Emacs and some
   command-line based build system for my development
   environment. In this article I describe the alternatives to the
   official Arduino IDE.
:date: 08/28/2014

.. figure:: {filename}/images/arduino-kit.jpg
   :width: 250 px
   :figclass: figure-to-right
   :alt: The Arduino Starting Kit (box)

A few days ago, we purchased an Arduino in order to improve our
knowledge about electronics and *low-level* and - why not? - be ready
for possible new incoming projects for customers. From the business
point of view, there are many interesting fields that are full of
opportunities like home automation, wireless sensor networks and
pervasive computing.

Personally, I find quite exciting developing something that actually
can change the environment and the state of other devices. Even more,
I find the fact of understanding the whole scenario really
interesting, from the electronics foundations to the source code, and
create new experiments from it.

Maybe it is not your case but electronics, assembly and, in general,
low-level programming have been my personal unresolved business:
something that I learnt wrong and still is. If that is your case,
maybe you can buy one of those Arduino starter kits available in the
market, like the `one we bought
<http://arduino.cc/en/Main/ArduinoStarterKit>`_. We selected this kit
because:

* It is the official one and, therefore, your money will contribute to
  the original project.

* Includes many different electronic components; from basic ones,
  like LEDs and resistors, to a bit more sophisticated like servo or
  LCD display. Very well packed and sorted. Different kind of
  components are grouped in separated boxes which is convenient.

* A beginners guide is shipped within the kit and makes your first
  experiences easy going. This guide is also helpful for understanding
  electronic foundations and describe how to build many different
  projects. All electronic components required for building the
  projects are included and also any extra part you would need like
  screws, cables, etc.

So, basically, the Starter Kit includes an Arduino UNO whose main
component is the ATMEL ATMega microcontroller. The model included in
our kit is the 328P-PU and their main hardware characteristics are:

* 14 Digital I/O Pins

* 8 Analog Inputs

* 32KB Flash Memory

* 1KB EEPROM

* 2KB RAM

* 16Mhz Clock Speed

Obviously, this does not sound quite powerful and does not tell too
much for a beginner like me. I will try to describe the architecture
internals in later posts. But for this review, it is enough to say
that, compared to other models, this microcontroller is not too bad.

Here there is a picture from our Arduino Starter Kit and you can see
how it looks:

.. figure:: {filename}/images/arduino-kit-2.jpg
   :width: 550 px
   :figclass: figure-full-center
   :alt: The Arduino Starter Kit (inside)

As you can see, the kit includes a small wood-made platform for fixing
Arduino board and breadboard which is quite handy during
prototyping.

I think this is enough for saying hello to Arduino. I will carry on
writing more posts about things I find out during my learning
process. If you also have enough motivation to start with Arduino, I
encourage you to buy one Arduino Starter Kit and start to learn and
enjoy.

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:
