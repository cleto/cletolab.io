.. -*- mode:rst; coding:utf-8 -*-

Development environment for Arduino (command-line)
##################################################

:tags: arduino,make
:category: programming
:slug: arduino-development-environment
:author: cleto
:summary: Let's start with Arduino basics. Maybe you have realised
          that Arduino provides their own IDE based Java. In this
          tutorial we will learn how to avoid this "Java-thing" and
          use standard tools like Make.
:date: 08/28/2014

The first question I wondered when started with Arduino was: what
tools should I use for creating programs? I have not too much
experience on software development for microcontrollers, but I guessed
I had to install some command-line tools like a cross-compiler, a
program uploader, etc. My first search on the Internet revealed that a
lot of people are using the Arduino IDE, which is packaged on Debian:

::

 Package: arduino
 ...
 Depends: default-jre | java6-runtime, libjna-java, librxtx-java (>= 2.2pre2-3), arduino-core (= 2:1.0.5+dfsg2-4)
 Recommends: extra-xdg-menus, policykit-1
 ...

Take a look at those dependencies. Arduino IDE is a Java-based
framework for creating programs in a *user-friendly* way. I suppose
I'm not *user-friendly* because this kind of applications make my work
experience worse. I'm used to working with Emacs, Makefiles and
command-line tools. You can create scripts and programs for automating
tasks easily and save time during development process effectively. So
I decided to avoid using this Arduino IDE and took a look at
*arduino-core* package:

::

 Package: arduino-core
 Source: arduino
 ...
 Depends: gcc-avr (>= 4.7.0), avrdude, gcc, avr-libc (>= 1.8.0)
 Suggests: arduino-mk
 ...

There they are. In fact, Arduino IDE uses command-line based programs
under the hood like the AVR backend for GCC (*gcc-avr*) or *avrdude*
for loading programs into the microcontroller. What about the
suggestion *arduino-mk*? This package contains macros and utilities
for creating Makefiles for Arduino projects. It is really helpful for
automating the building process and the software upload. The upstream
project is called `Arduino-Makefile
<https://github.com/sudar/Arduino-Makefile>`_.

Excellent! Now we know the tools we could use for setting up our
environment. I'll create a very simple project using *arduino-mk* just
to show how to use it within a Makefile. Let's implement the very
basic project called `Blinking LED
<http://arduino.cc/en/Tutorial/Blink?from=Tutorial.BlinkingLED>`_. This
is the first project included in the Arduino Project Book.

Once you have built the circuit, let's compile and execute the code. I
assume you have a directory with your Arduino experiments at
*~/devel/arduino*:

::

   $ cd ~/devel/arduino
   $ mkdir blink-led

Now create the source code file of the application in the file call
*main.cpp* (or *main.ino*):

.. code-block:: cpp

   /*
    Blink
    Turns on an LED on for one second, then off for one second, repeatedly.
    This example code is in the public domain.
   */

   // Pin 13 has an LED connected on most Arduino boards.
   // give it a name:
   int led = 13;

   // the setup routine runs once when you press reset:
   void setup() {
     // initialize the digital pin as an output.
     pinMode(led, OUTPUT);
   }

   // the loop routine runs over and over again forever:
   void loop() {
     digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
     delay(1000);               // wait for a second
     digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
     delay(1000);               // wait for a second
   }

Looks like C or C++ but simpler, doesn't it?. Later we'll see why. Then,
create the Makefile using Arduino-Makefile:

.. code-block:: makefile

   BOARD_TAG = uno
   MONITOR_PORT = /dev/ttyACM0

   include /usr/share/arduino/Arduino.mk

   clean::
   	$(RM)  *~

Note that `clean` target is used with two-colon notation `::`. This
means that when you execute `make clean`, all actions for cleaning at
`Arduino.mk` will be executed and, *then*, your local actions.
Current version of Arduino-Makefile does not support this
feature. However, I suggested upstream to modify the `clean` target by
simply adding `::` at `Arduino.mk` file. `They accepted this solution
<https://github.com/sudar/Arduino-Makefile/issues/239>`_ and it will
be included in following releases of Arduino-Makefile.

To be fair, `BOARD_TAG` or `MONITOR_PORT` are not required because
those values are set by default. However, it is good to know where
they come from. Execute the following for listing all boards supported
by Arduino-Makefile:

::

   $ make show_boards
   ...
   pro328        Arduino Pro or Pro Mini (3.3V, 8 MHz) w/ ATmega328
   pro5v328      Arduino Pro or Pro Mini (5V, 16 MHz) w/ ATmega328
   pro5v         Arduino Pro or Pro Mini (5V, 16 MHz) w/ ATmega168
   pro           Arduino Pro or Pro Mini (3.3V, 8 MHz) w/ ATmega168
   robotControl  Arduino Robot Control
   robotMotor    Arduino Robot Motor
   uno           Arduino Uno

Where do they come from? Take a look at
*/usr/share/arduino/hardware/arduino/boards.txt*. In that file you can
see all the specifications for each supported Arduino platform. The
prefix of each property is what is used as values of `BOARD_TAG`.

Now build the project just using:

::

   $ make
   ...
   AVR Memory Usage
   ----------------
   Device: atmega328p

   Program:    1206 bytes (3.7% Full)
   (.text + .data + .bootloader)

   Data:         11 bytes (0.5% Full)
   (.data + .bss + .noinit)

Interesting. At the end of the compilation, some useful stats are
shown. It seems that a lot of C++ code has been compiled. That is the
core library of Arduino that makes user programming easier. They
provide many structures that actually uses AVR-specific code in order
to elevate the abstraction level. Hum! We have to find out a bit more
about this.

Ok, let's program the Arduino. Make sure you have plugged your Arduino
and that */dev/ttyACM0* exists. Then:

::

   $ make upload

And that's all. If everything was fine, your LED will blink
periodically. As you can see, thanks to these tools, developing
Arduino programs using command-line tools is not too hard. I will use
this method because it fits to my requirements very well, although
still have some questions about the building process: Can we analyse
what object code is actually generated? Are we able to write assembly
code?. Wow!  I have plenty of questions. I suspect that this is only
the beginning.

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:
