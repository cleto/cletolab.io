.. -*- mode:rst; coding:utf-8 -*-

Under the hood of Arduino (I): the building process
###################################################

:tags: arduino,make,binutils
:category: programming
:slug: arduino-under-the-hood-01
:author: cleto
:summary: Let's see what it is going on behind Arduino C++ library in
          order to understand more the actual Arduino's architecture
          and related development tools.
:date: 09/07/2014

In previous posts, we `set our environment up
<{filename}../environment-configuration.rst>`_ and we used
`Arduino-Makefile <https://github.com/sudar/Arduino-Makefile>`_ in
order to build and upload binary code into our Arduino
micro-controller. We also used the Arduino C++ library for creating
our first program which was so simple: just blinking a LED. However,
we noticed that a lot intermediate steps were executed during building
process and that made me wonder what is going on inside the building
process of an Arduino program.

Let's start with a simple example for blinking the built-in LED of the
Arduino board:

.. code-include:: blinking-led-arduino/main.cpp
   :lexer: cpp

How do I know that pin `13` is connected to an LED? Well, you may want
to check `the Arduino UNO board description
<http://arduino.cc/en/Main/arduinoBoardUno>`_ and read the brief
description about what devices are included within the board of
Arduino. I would recommend to search this LED into `the schematic
<http://arduino.cc/en/uploads/Main/Arduino_Uno_Rev3-schematic.pdf>`_,
so you can get used to read this kind of diagrams.

OK, let's execute `make` and examine, step-by-step, what happens.

Source code compilation
-----------------------

The first step is the *compilation* of the C++ source code *and* the
Arduino library by using `avr-g++` cross-compiler. Let's have a look
at one of the output lines during the execution of `make`:

::

 /usr/share/arduino/hardware/tools/avr/bin/avr-g++ -x c++ \
   -include Arduino.h \
   -MMD \
   -c \
   -mmcu=atmega328p \
   -DF_CPU=16000000L \
   -DARDUINO=105 \
   -D__PROG_TYPES_COMPAT__ \
   -I. \
   -I/usr/share/arduino/hardware/arduino/cores/arduino \
   -I/usr/share/arduino/hardware/arduino/variants/standard \
   -Wall -ffunction-sections -fdata-sections -Os -fno-exceptions \
   main.ino \
   -o build-uno/main.o


Wow! There are many options! Let's study some of them in more detail:

- **mmcu**: it tells to the compiler what concrete AVR architecture
  should be used.

- **MMD**: this is only a convenience for make. This option generates
  all those `.d` files you can find within `build-uno` directory.

- **D**: following symbols are defined:

  * *F_CPU*: CPU frequency. In this case 16 MHz. This is used across
    the Arduino C++ library.

  * *ARDUINO* and *__PROG_TYPES_COMPAT__*: I have not found where
    these symbols are used. Any comment/help?

The way it is compiled looks interesting to me. First of all, my
`main.ino` program is translated to object code and then, all required
source code from Arduino library is also compiled to object code.

Building a static library
-------------------------

Once all `.o` files have been generated, then `avr-ar` is called to
*generate a static library* which will include every object file from
the Arduino library and is called *libcore.a*. Note that our `main.o`
is *not* included in this static library. You may find all these
generated files within `build-uno/core` directory.

::

 /usr/share/arduino/hardware/tools/avr/bin/avr-ar \
  rcs \
  build-uno/libcore.a \
  ...many paths to .o files from Arduino library...

GNU ELF binary generation
-------------------------

Now we have `libcore.a` and `main.o` generated. Then, `avr-gcc` is
called for *creating the binary*:

::

 /usr/share/arduino/hardware/tools/avr/bin/avr-gcc \
  -mmcu=atmega328p \
  -Wl,--gc-sections \
  -Os \
  -o build-uno/blinking-led-arduino.elf \
  build-uno/main.o \
  build-uno/libcore.a \
  -lc -lm

In my case, the generated file is called `blinking-led-arduino.elf`
because that is the project directory name and `make` uses it as
output file name. This is a proper executable for Atmel AVR
micro-controllers as you might check using `file` command:

.. code-block:: console

 $ file build-uno/blinking-led-arduino.elf
 build-uno/02.elf: ELF 32-bit LSB executable, Atmel AVR 8-bit, version 1 (SYSV), statically linked, not stripped

Tweaking the GNU ELF file
-------------------------

Could the GNU ELF file be uploaded directly into the Arduino board?
No. Despite the GNU ELF file is a valid binary for AVR
micro-controllers, the Arduino programmer, that is, that application
that actually puts the binary code within micro-controller, does
**not** accept a GNU ELF binary file. For that reason, a couple of
tweaks are done by using a tool called `avr-objcopy`:

- Extraction of the *EEPROM* section into the file `.eep`. `-j`
  option let us select the section of the binary we want to export
  and `-O` option specifies the output format.

- Extraction of the application binary code but *EEPROM* section
  into the file `.hex`.

`.hex` file will be used by `avrdude` for uploading the program into
Arduino. Our example program does *not* use EEPROM memory, so in this
example `.eep` is never used.

Statistics
----------

Last step is just a call to `avr-size` to show some useful statistics
about the ELF file:

::

 AVR Memory Usage
 ----------------
 Device: atmega328p

 Program:    1024 bytes (3.1% Full)
 (.text + .data + .bootloader)

 Data:          9 bytes (0.4% Full)
 (.data + .bss + .noinit)

Two different sizes are highlighted in the output:

* Program size: 1024 bytes. This is the result of the sum of the size
  of three sections: *.text* section (assembly code of our program),
  *.data* section (constants and statically-allocated variables) and
  *.bootloader* section.

* Data size: *.bss* section is part of *.data* section and contains
  all those uninitialised global or static variables. Variables
  included within *.bss* section will be initialised to 0 at start-up
  time. However, any variable stored in *.noinit* section will not be
  zero-initialised.


Playing with the ELF file
-------------------------

The ELF file could be examined using the standard tools like `objdump`
but in AVR version (e.i. `avr-objdump`). This tool is very helpful for
compiler developers but I think sometimes it could be handful for
application programmers:

* Checking the **symbol table**

  .. code-block:: console

     $ objdump -t build-uno/blinking-led-arduino.elf
     # ... many rows of the symbol table ...
     000000e8 g     F .text	00000028 loop
     000000dc  w      .text	00000000 __vector_12
     000000dc g       .text	00000000 __bad_interrupt
     00000400 g       *ABS*	00000000 __data_load_end
     000000dc  w      .text	00000000 __vector_6
     00000068 g       .text	00000000 __trampolines_end
     000000dc  w      .text	00000000 __vector_3
     00000370 g     F .text	0000006c digitalWrite
     000002fe g     F .text	00000072 pinMode
     # ... more rows of the symbol table ...

  Obviously, each column tells something about the symbol located at
  the end of the row. For instance, symbol `digitalWrite` is
  identified by `00000370` within symbol table, it is global (`g`)
  function (`F`) and its size is `6c` bytes. Look at `man avr-objdump`
  for getting more details about this column format.

* Studying the **assembly code**:

  .. code-block:: console

     $ objdump -S  build-uno/blinking-led-arduino.elf
     # ... loads of assembly code ...
     000000e8 <loop>:
     e8:   61 e0           ldi     r22, 0x01       ; 1
     ea:   8d e0           ldi     r24, 0x0D       ; 13
     ec:   0e 94 b8 01     call    0x370   ; 0x370 <digitalWrite>
     f0:   64 ef           ldi     r22, 0xF4       ; 244
     f2:   71 e0           ldi     r23, 0x01       ; 1
     f4:   80 e0           ldi     r24, 0x00       ; 0
     f6:   90 e0           ldi     r25, 0x00       ; 0
     f8:   0e 94 f5 00     call    0x1ea   ; 0x1ea <delay>
     fc:   60 e0           ldi     r22, 0x00       ; 0
     fe:   8d e0           ldi     r24, 0x0D       ; 13
     100:   0e 94 b8 01     call    0x370   ; 0x370 <digitalWrite>
     104:   64 ef           ldi     r22, 0xF4       ; 244
     106:   71 e0           ldi     r23, 0x01       ; 1
     108:   80 e0           ldi     r24, 0x00       ; 0
     10a:   90 e0           ldi     r25, 0x00       ; 0
     10c:   0c 94 f5 00     jmp     0x1ea   ; 0x1ea <delay>
     # ... even more assembly code ...

  Sweet! I do **not** understand AVR assembly yet but I can easily see
  our loop function in the previous code: `digitalWrite()`, `delay()`,
  another `digitalWrite()` call and then another `delay()` call.

All this information is extracted from the ELF file. Linker program
could provide us more information about different ELF sections and the
symbol table in a *map file*. However, this file is not generated
unless we explicit ask for it during linking time. So let's re-call
the linker for generating a map file:

.. code-block:: console

 $ avr-gcc -mmcu=atmega328p -Wl,-Map,build-uno/blinking-led-arduino.map -Os -o build-uno/blinking-led-arduino.elf build-uno/main.o build-uno/libcore.a -lc -lm

Then, file `build-uno/blinking-led-arduino.map` is created and part of
its content looks like the following:

::

 .text.loop     0x00000000000000e8       0x28 build-uno/main.o
                0x00000000000000e8                loop
 .text.__vector_16
                0x0000000000000110       0x94 build-uno/libcore.a(wiring.o)
                0x0000000000000110                __vector_16
 .text.micros   0x00000000000001a4       0x46 build-uno/libcore.a(wiring.o)
                0x00000000000001a4                micros
 .text.delay    0x00000000000001ea       0x4c build-uno/libcore.a(wiring.o)
                0x00000000000001ea                delay
 .text.init     0x0000000000000236       0x76 build-uno/libcore.a(wiring.o)
                0x0000000000000236                init
 .text.turnOffPWM
                0x00000000000002ac       0x52 build-uno/libcore.a(wiring_digital.o)
 .text.pinMode  0x00000000000002fe       0x72 build-uno/libcore.a(wiring_digital.o)
                0x00000000000002fe                pinMode
 .text.digitalWrite
                0x0000000000000370       0x6c build-uno/libcore.a(wiring_digital.o)
                0x0000000000000370                digitalWrite

By using this map file we are able to find out in which file a certain
symbol was defined. For example, `micros` (which is included in the
`.text` section) comes from `wiring.o` file.

Well, we have taken a look at the building process and some
"low-level" tools for studying some generated files. In the next post,
I will create a version of this blinking LED program just using the
AVR C library. Obviously, I will not try to do the same as it is
already done in the Arduino C++ library (aka, *reinventing the
wheel*). My purpose is just doing an exercise for learning more about
ins and outs of the micro-controller and for knowing different ways to
program it.


References
----------

* `Memory sections
  <http://www.nongnu.org/avr-libc/user-manual/mem_sections.html>`_
* `Special sections of the Linux Base Core Specification
  <http://refspecs.linuxfoundation.org/LSB_4.1.0/LSB-Core-generic/LSB-Core-generic/specialsections.html>`_
* `A project for AVR micro-controllers
  <http://www.nongnu.org/avr-libc/user-manual/group__demo__project.html>`_
* `objdump examples
  <http://www.thegeekstuff.com/2012/09/objdump-examples/>`_

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:

..  LocalWords:  binutils
