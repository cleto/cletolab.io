// -*- mode:c++; coding: utf-8 -*-

/*
  Copyright (C) 2014 Cleto Martin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  A traffic light simulation. If a pedestrian pushes the button, then
  it will turn red but not straightaway. Using the same circuit as in
  02, just replaces the second red LED by a yellow one.
*/

// Modify this constant to change the time of the traffic light life
// cycle. Other time intervals are calculated from this. All units are
// seconds.
const float MAX_CYCLE_TIME = 20.0;
const float MAX_GREEN_TIME = MAX_CYCLE_TIME * .5;
const float MIN_GREEN_TIME = MAX_GREEN_TIME * .5;

enum Status {GREEN, YELLOW, RED, RESET};

// Depending on whether pedestrian request way or not, this variable
// will hold the amount of seconds that green light will be shown
float current_green_time = MAX_GREEN_TIME;

float current_time = .0;

// Has the pedestrian requested way?. We will only allow requests
// during green light is on.
bool request = false;

void setup() {
    pinMode(2, INPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
}

void show_lights(Status st) {
    switch (st) {
    case GREEN:
        digitalWrite(3, HIGH); digitalWrite(4, LOW); digitalWrite(5, LOW);
        break;
    case YELLOW:
        digitalWrite(3, LOW); digitalWrite(4, HIGH); digitalWrite(5, LOW);
        break;
    case RED:
        digitalWrite(3, LOW); digitalWrite(4, LOW); digitalWrite(5, HIGH);
        break;
    case RESET:
        digitalWrite(3, LOW); digitalWrite(4, HIGH); digitalWrite(5, HIGH);
     }
}

float get_time_for(Status st) {
    switch (st) {
    case GREEN:
        return current_green_time;
    case YELLOW:
        return current_green_time + (MAX_CYCLE_TIME * .1);
    case RED:
        return current_green_time + (MAX_CYCLE_TIME * .4);
    case RESET:
        return current_green_time + (MAX_CYCLE_TIME * .5);
    }
}

void loop() {
    if (current_time >= get_time_for(RESET)) {
        current_green_time = MAX_CYCLE_TIME * .5;
        request = false;
        current_time = 0;
    }

    request |= digitalRead(2);

    if (request and current_time < current_green_time) {
        current_green_time = (current_time < MIN_GREEN_TIME) ?
            MIN_GREEN_TIME : current_time;
    }

    for (int i=0; i<4; ++i) {
        if (current_time < get_time_for(static_cast<Status>(i))) {
            show_lights(static_cast<Status>(i));
            break;
        }
    }

    delay(100);
    current_time += .1;
}
