:title: Implementing a traffic light with Arduino
:tags: arduino, C, C++, experiments
:category: programming
:slug: traffic-light-arduino
:author: cleto
:summary: This was my first Arduino project. Very simple and easy to
    understand: the implementation of the behaviour of a typical
    traffic light with a button for pedestrian.
:date: 14/1/2017

Almost 3 years later I have finally decided to publish something and
it is not new at all. Back in 2014 I built my first Arduino project
inspired by the one of the projects proposed in the Arduino projects
book. Today, doing some cleaning at home, I found it in a drawer of my
desk and I thought it would be so embarrassing not finishing what I
started long time ago.

So, there we are. The idea was to implement a traffic light that
allows to pedestrian to ask for way using a button.

The circuit
-----------

The design is based on the project #2 of the Arduino projects book
and it is very similar to it. We are only changing the LED colours and
the logic.

I have used `Fritzing <http://fritzing.org>`_ for creating the circuit
diagram. It is a pretty good tool with lots of components already
included and many other features like automatic schematic and PBC
generation. There is a code editor that allows you to save the code
and upload it to the Arduino, although I haven't used it. It also
works for other platforms like Rapsberry PI and it is great for
storing a whole project all in the same file (circuit design, code ,
etc.). You can find lots of projects in their website ready to upload
to your Arduino.

For the traffic light, the design of the circuit looks like this:

.. figure:: {filename}/images/traffic-light_bb.svg
   :figclass: figure-full-center


The code
--------

The logic of a traffic light is simple enough to be coded
quickly. However, the fact that there is input from pedestrians
complicates. Let's explain how the traffic light should work:

.. role:: red
.. role:: yellow
.. role:: green

* The regular light cycle will be :green:`GREEN` -> :yellow:`YELLOW`
  -> :red:`RED` -> :red:`RED` / :yellow:`YELLOW` (and then back to
  :green:`GREEN`). This is the way traffic lights work in the UK.

* The duration of each stage can be set proportionally to
  a `MAX_CYCLE_TIME`. For instance, if `MAX_CYCLE_TIME` is 20
  seconds, we can set that the time the traffic light will
  be :green:`GREEN` is 50% of the `MAX_CYCLE_TIME` (that is, 10
  seconds), :yellow:`YELLOW` can be 10% and :red:`RED` might be 40%.
  We will call `RESET` to the status before moving back to
  :green:`GREEN` (when :red:`RED` / :yellow:`YELLOW` are on).

* The button could be pressed at any time. However, the traffic light
  must only take care about it if the status is :green:`GREEN`,
  so we will ignore those requests coming from other statuses and also
  duplicate requests.

* A naive implementation could allow pedestrian to block car traffic
  all time by continuously pushing the button (hmm... is that really a
  bad thing? :-) ). In order to prevent this, we will set a
  `MIN_GREEN_TIME` on which even we have received a request, we will
  wait for at least that time to be green and allow a certain amount
  of cars can circulate.

With these rules in mind, one possible implementation is as follows:

.. code-include:: src/main.ino
   :lexer: cpp

The idea behind this implementation is that the `loop` just switches
on whatever lights are supposed to be switched at each time (function
`get_time_for()`). Since we have defined the time ranges, lights will
be shown appropriately using the function `show_lights()`.


It works!
---------

Well, that's it. Not really exiting project but a good starting point
for learning some electronics and discover new tools and features from
the Arduino's world.

.. raw:: html

    <center>
    <video controls src="https://lh3.googleusercontent.com/dJpyVnQabnD5wZYLCaecMSlf9OXzMuK_05EHS0cbUA1p9godFDuuXtyhmJBCq090DfykSrzT2wqQb4nLWQLUjwzvJN_IwPnxouP1RHvBmjuHHxIAfPH952Ag2IC4w6z7Mt73k949upi8luu0F0CUk9xe2hVqPmVVhq7DYaP7SNPGVUupYETxCGS32jgcU24eTf8WZ7qfsEzBeWNk_ye7DCdFlsG0Kvmzz4txVa7umqiD9nXGVM5ik6-UN1s1NgcHZiSQYOwzLrncojXZg6p0_O2qxp96Q8SpELbSxfcHunvFn_ZhDSTeyxEUuH3-_YKUAeBG77R1l3IRZjs-pFFWMdW9gPIlm7YKgbvp5rdutj_dsv5ZeWmd7ztza_aFK1i3iyMfxsSyHjm6YPGexdGmorrhO2gUTSgPpYIYtIdpcjsL1Lz6mTAyc5modwYqhCSctVNK0Ooej4L5-bsu-_AbfSsvCtxTBSO5boWdLffDnZia2LdXghcR54S6IQ7DvU3HwStwZ8I9auMhCkOl22F9fKGVVT212sUTSOBbKQiZIA1Et4c3BafvpydIr6bIP-aPclQ2ucgwyvPUSMFwMcFbVMDQQK-5kc3WTn9Wc8R2ITiKh_EVBKoWeg=m18?cpn=fDdN1F21EtxHYf-g&c=WEB&cver=1.20170112"></video>
    </center>
