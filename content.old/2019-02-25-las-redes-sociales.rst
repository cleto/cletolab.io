.. -*- coding: utf-8; mode:rst-mode -*-

:title: Las redes sociales
:slug: social-networks
:lang: es
:translation: true
:tags: opinion,controversy
:category: opinion
:authors: cleto
:date: 25/02/2019
:summary: Hay mucha gente que me pregunta por mi contacto en Facebook,
	  o si me pueden enviar algo por WhatsApp. Al responder que no
	  uso ninguna de esas cosas muestran su cara de asombro. Aquí
	  explico qué opino de las redes sociales y por qué no uso
	  algunas de ellas.

En primer lugar, me gustaría pedir disculpas al lector que espera que
este artículo no exceda los 140 caracteres. Como en casi todo, la cosa
es compleja, difícil de argumentar, y carezco de la chispa para
sintetizar mis ideas en unos cuantos mensajes cortos.

Las redes sociales, aunque habría que definir qué son de forma
precisa, son una herramienta muy útil para la comunicación entre
personas. Aunque esto lleva muchos años resuelto desde la invención
del email y los primeros sistemas de chat (como las BBSes_ o IRC),
nadie llamaba a este tipo de sistemas "redes sociales", principalmente
porque su uso estaba restringido a gente que realmente estaba
interesada en este tipo de comunicación.

Actualmente, el concepto se asocia normalmente a plataformas tipo
Facebook, Instagram, Twitter, LinkedIn, y demás. Existen diferencias
entre ellas, pero básicamente son sistemas para proporcionar
comunicación entre personas. Con la llegada de los smartphones estos
sistemas se popularizaron completamente. Aún recuerdo cuándo conocía
algunas personas que usaban Facebook (normalmente aquellos que
residían en el extranjero) y que era una plataforma completamente
desconocida en España (por aquellos años se llevaba Tuenti).

Así que aquí estamos. Somos la primera generación de seres humanos que
disfrutamos de un lujo semejante. Nadie jamás en la Historia pudo
comunicarse con otro ser humano con la rapidez, los medios y la escala
a la que podemos hacerlo hoy desde el bar mientras tomamos un
café. Pero hay que recordar que seguimos siendo lo mismo: seres
humanos. Y esto no va a cambiar tan rápidamente.

Las redes sociales no sólo nos conectan de una forma no antes vista
sino que amplifican nuestras **carencias** y, lo que es peor, se
quedan **registradas**. Por ello, me gustaría analizar algunos de los
graves problemas que, desde mi humilde opinión, veo a mi alrededor
cuando la gente usa este tipo de tecnologías. Quede dicho de antemano
que no soy un gran usuario de las mismas y que mi crítica es
enteramente constructiva con la idea de que el lector pueda
reflexionar sobre su propia conducta, al igual que lo he hecho yo. Si
el lector se ofende (tan habitual en las redes sociales), lo lamento.


El test de la plaza del pueblo
------------------------------

En muchas ocasiones he preguntado a mis amigos por qué han publicado
cierto tipo de comentarios. Es curioso pero, a veces, me cuesta
reconocer a mi propios amigos haciendo cierto tipo de
afirmaciones. Más que nada, por su falta total de "vergüenza
ajena". He visto publicadas ciertas cosas que jamás he hablado en
persona, o que tienen el menor interés, o que son afirmaciones
extremadamente imprudentes.

Para hacer más evidente lo que quiero decir, te propongo pasar el
*test de la plaza del pueblo* a lo que vayas a publicar la próxima
vez. Es bastante sencillo. Supongamos que quieres publicar algo en una
red social. Haz el siguiente experimento mental: te subes a un sitio
alto en la plaza de tu pueblo (o tu ciudad, si no eres de pueblo) y
con un proyector en mano (para mostrar imágenes y vídeos) gritas a los
cuatro vientos tu publicación. ¿Lo harías en ese caso? Si respondes
que no o dudas, entonces no lo publiques.

Por ejemplo, ¿algunas de estas publicaciones pasan el test?
(se deja al lector que responda):

- "Yendo para Madrid a pasar una tarde con mis hijos en el zoo"

- "Aquí tomando una cañita con mi buen amigo/a"

- "Políticos de mierda, no hacéis más que robar"

El test de la plaza del pueblo es muy útil por los siguientes motivos:

* Hace patente que lo que se dice va a ser escuchado por gente de
  verdad, que no es un mensaje al aire. Estar diciendo algo en público
  conlleva una responsabilidad mínima sobre lo que se dice (moral y
  legal). Sé prudente.

* Las publicaciones dicen más de lo que parece. La hora en que se
  publica, los objetos que salen en la foto, la localización
  GPS,... todo ese también se publica. Y, seguramente, no tienes nada
  que esconder pero, ¿realmente has pensado en todas que se pueden
  usar ese tipo de datos y sus consecuencias posibles? Te lo digo yo,
  no. Así que, por favor, Sé prudente.

* En el público hay personas que te pueden conocer o no. Puede ser que
  para ti no sea un problema lo que piense la gente que no te conoce,
  pero de entre la que no te conoce está gente que podría influir
  mucho en tu vida (futuros empleos, proyectos, amistades, etc.). Sé
  prudente.

* Entre el público hay gente con diferentes sensibilidades y
  opiniones. Normalmente, no nos gusta herir gratuítamente las
  sensibilidades de otros (sobretodo con comentarios que no se han
  pensado lo suficiente). Además de esto, también puede haber gente
  que sepa mucho del tema del que estás opinando. Asume que no lo
  sabes todo y, de nuevo, sé prudente.

Así que, como se puede ver, este sencillo test trata de evitar que
seas lo que siempre en mi pueblo se ha conocido como un boceras_.

La inmediatez
-------------

"Uff... que largo es lo que me has mandado. Resumen, por favor". Esto
me lo han pedido varias veces, y personas muy formadas, ante un texto
que superan los 2 o 3 párrafos. En Amazon se puede comprar con un
click y tenerlo en unas horas en tu casa. "Sí, hay que hacerlo
rápido", pensará Amazon. "No sea que piensen y se les pase su ansia
consumista".

Todas las redes sociales tienen sistemas de notificaciones. "Alguien
le ha gustado la foto" o "alguien ha visto tu perfil". Todo
exquisitamente diseñado para mantener la sensación de la inmediatez en
la comunicación. "Si el tick del chat no sale, algo falla". ¿Es
necesario estar pendiente de toda esta carga social? ¿Para qué?

La inmediatez no sería un gran problema si se quedara en esto. Lo
verdaderamente peligroso es que genera gente vaga intelectual y poco
crítica. Las fake news es básicamente explotar esta debilidad en las
personas y, en este caso, la inmediatez no es algo bueno: no
genera una buena información y expone a la gente a ser manipulada.

La errancia
-----------

Ligado con lo anterior, la errancia, el ir de una cosa a la otra, de
un tema a otro, es una constante en este tipo de plataformas. Si ya en
un periódico esto es muy evidente (cada unas pocas páginas se cambia
de tema continuamente), en el caso de las redes sociales este cambio
de contexto se produce constantement y a la misma velocidad con la que
se mueve la rueda del ratón (como en el caso de Twitter). La idea es
que no se piense algo demasiado, que no se profundice en lo que se ve
y se quede con el titular.

El filósofo Martin Heidegger decía que la errancia era parte de una
existencia *inauténtica*, ya que el ser inauténtico va de una cosa a
la otra sin profundizar en ningún tema. Básicamente, nada sorprende ni
nada tiene ningún misterio. Todo es sencillo, fácil y simple de
entender. ¿Te suena todo esto a algo? La errancia no es buena para
ti. Echa un vistazo a `este artículo`_ sobre este tema.

Los 2 minutos de odio
---------------------

¿Qué plan puede haber mejor que una buena sesión de odio al otro, en
pequeñas dosis diarias para mantenerlo bien alto? Estos temas
"facilones" de los que todo el mundo parece que puede hablar y tener
opinión son los más proclives a tener una inmensidad de gente
insultándose los unos a los otros.

Es curioso que no haya este tipo de debates sobre, digamos, física
cuántica. ¿Quizás es porque ahí no es tan fácil opinar? ¿No será
quizás que muchos debates de este estilo son extremadamente complejos
y posiblemente sin una buena solución?

Y evidentemente, las fuerzas políticas saben todo esto y lo usan. Unos
mejor que otros, los partidos políticos (sobre todo los más radicales)
utilizan este tipo de enfrentamiento para mantenerse vivos. Necesitan
del odio en el otro, en el que opina diferente, para poder
subsistir. Y si no hay un otro, se crea. Esto, que se ha usado a la
largo de la historia, no es nada nuevo. Las redes sociales, junto a
los medios de comunicación, han permitido que la gente se odie
anónima, más rápida y cómodamente desde el salón de su casa.

Tú eres el producto
-------------------

En una `entrevista`_, Richard Stallman definió de una forma
extraordinaria este fenómeno: "Facebook no tiene usuarios. Tiene
usados". Es difícil decir tan claramente lo que realmente las redes
sociales hacen con sus "usuarios".

Para ver con más claridad este problema, pensemos en Facebook, que es
probablemente el sistema distribuído más grande del mundo
actualmente. El número de ordenadores necesario para poder dar
servicio a miles de millones de usuarios es, como uno se puede
imaginar, alto. Muy alto. Estamos hablando de muchos muchos
miles. Estamos hablando de sus propias naves industriales llenas de
aire acondicionado, personal y ordenadores funcionando las 24
horas. Pues bien, sólo pensando en la cantidad de recursos que se
necesitan para poder mantener vivo Facebook, ¿cómo es posible que sea
gratis utilizarlo? Y lo mismo ocurre con Gmail, por ejemplo. Cualquier
proveedor de correo electrónico cobra por su servicio (porque require
recursos, esfuerzo, tiempo, etc.) ¿Y cómo es que no cobran?

La razón está dentro de esta pregunta: en la ganadería, ¿les cobramos
a los animales la comida?

El vértigo al aislamiento
-------------------------

Utilizando técnicas muy depuradas para captar la atención y enganchar
a las personas, han conseguido *hackear* al cerebro humano. Hay
empresas que se dedican a `esto`_. Es ya un negocio sobradamente
conocido y el hecho de manipular el comportamiento de los usuarios, a
través de diferentes técnicas, es algo "normal".

El zénit del control sobre el usuario es cuando éste trata de dejar de
usarlo y experimenta el temor de quedarse aislado. El hackeo ha
llegado hasta tal punto que:

1) Se ha hecho viral. La gente lo usa, tú lo usas. ¿Cómo no lo vas a
   usar? ¿Y si no te enteras de lo que ocurre?

2) Forma parte de la vida diaría (casi diría horaria) que desprenderse
   de ello provoca desasosiego o aflicción. ¿Por qué tengo que dejarlo
   si me gusta?

¿Y tú por qué tienes Twitter?
-----------------------------

Actualmente estoy presente en Twitter, en LinkedIn y en Telegram. El
uso que le doy a la primera y a la segunda es testimonial porque no
veo nada interesante. LinkedIn se ha hecho bastante necesario para el
mundo laboral, donde mucha gente trata de contactar contigo
directamente por este medio. Es por ese motivo por el que estoy y
permito que me utilicen.

Telegram (y otros protocolos de chat como IRC, Jabber, etc.) sí que lo
uso más habitualmente para estar en contacto con familiares y
amigos. La presión para cambiarme a WhatsApp es continua y
terriblemente agobiante. Afortunadamente, la gente que realmente me
importa siempre ha accedido a contactar por los medios que les he
sugerido, pero entiendo que este es un gran problema para muchos que
se ven incapaces de asumir el aislamiento que produce no utilizar
WhatsApp. Pero si valoras tu tiempo, ánimo, es la mejor decisión que
puedes tomar. Piensa que la gente verdaderamente importante en tu vida
seguirá en contacto contigo.

He utilizado Facebook en el pasado y dejé de hacerlo, entre múltiples
razones, porque empecé a pensar sobre todos los puntos
anteriores. Seguro que me pierdo muchas cosas, pero desde entonces he
descubierto otras muchas como leer libros_ :-).

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:

.. _BBSes: https://en.wikipedia.org/wiki/Bulletin_board_system
.. _libros: /pages/books.html
.. _boceras: http://dle.rae.es/?id=5j9CcUm
.. _este artículo: http://vladimirvalladares.blogspot.co.uk/2015/02/las-habladurias-de-heidegger.html
.. _entrevista: https://retina.elpais.com/retina/2019/02/22/tendencias/1550819915_405396.html
.. _esto: https://www.boundless.ai
