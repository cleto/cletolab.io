.. -*- mode:rst; mode:flyspell; coding:utf-8 -*-

Final project about Plato's Republic
####################################

:date: 2017/02/04
:tags: philosophy
:category: philosophy
:slug: platos-republic-overview
:author: Cleto Martin
:summary: I'm finishing a very interesting course in coursera.org
   called "Plato & His Predecessors" and, as a final project, I have
   to write about Plato's Republic. This is what I sent as exercise.

First of all, I would like to apologise to any reader that actually
knows something about Plato and philosophy. Since around 1 year, I've
been reading about philosophy in general (introduction level for a few
topics like metaphysics), history of philosophy and just a couple of
original authors like Plato.

I'm just finishing a nice course called "Plato & His Predecessors" at
coursera.org. There is a couple of final projects you can choose in
order to finish it successfully: a continuation of a Plato's dialogue
or a letter to a friend explaining Plato's Republic. I have chosen the
latter since I read the Republic a few months ago, so I think it is a
good chance for revisit it and try to explain to this imaginary friend
what I have learnt during the course and what I remember from my
reading of the book.

On Plato's Republic: why is justice preferable to injustice?
============================================================

..
   Prompt 2: Justice in the Republic: Write a letter to a friend who has
   not read Plato’s Republic. Explain (a) what Socrates claims justice is
   (justice in a person) and (b) why he thinks it is never a good idea to
   be unjust.

Dear friend,

I have recently read The Republic (by Plato) and I would like to
explain something I have found really interesting to you. Although the
book is not a light reading and, for a beginner, many of the deeper
philosophical points might be difficult to discover, it is really
well-written and that makes The Republic a pleasing-to-read book.

The Republic is a fictional dialogue between Socrates (Plato's master)
and a few other Plato's contemporary people: Cephalus, Polemarchus,
Thrasymachus, Adeimantus, and Glaucon (the last two are Plato's
half-brothers). It is a pretty long dialogue (10 books) where the
characters mainly debate about the concept of justice. Specifically,
Socrates is keen on seeking a good definition of justice. And his
method, called the Socratic method, is based on asking and answering
questions between individuals. Thus, Socrates tries to point out
contradictions based on what people answer to his questions by asking
more questions and providing examples that show the answers are not
fully satisfactory.

Sometimes, the Socratic method exasperates interlocutors since it
seems to be an endless questioning process with no purpose. This is
what happens, for instance, at 336d (Book I) where Thrasymachus, who
until that moment was listening to the on-going debate about what
justice is, criticises Socrates' method vividly by arguing that
"asking questions is always easier than answering them".

Until that moment, Socrates was only criticising other's answers about
what justice is. A few examples of the answers given to Socrates are:

* Justice is telling the truth.

* Justice is giving back what has been received by others.

* Justice is harming your enemies and doing good to your friends.

All these answers are easily defeated by Socrates as he can show that
there are lot of examples where the opposite of each answer can be
considered just as well. For instance, if a friend asks you to guard
his weapons for a while and, suddenly, your friend is out of his mind
and ask for getting his weapons back, denying the access to the
weapons to somebody that is not in his right mind is not unjust (331c,
Book I).

Thrasymachus gives an interesting answer by saying that justice is to
follow the rules established by the most powerful entity (typically,
the state). However, as Socrates points out, governors can make
sometimes mistakes and following some rules might be unjust. Then, the
discussion follows to something deeper: is the injustice more
beneficial than justice? Why should we try to be just people if
injustice could be a better option as individuals? Thrasymachus thinks
that injustice is a better option, overall for those people that have
to obey the law since following it, that is, being just, is only
rewarding for those who imposed the law, not for those who obey it.

Inspired by this discussion, Glaucon and Adeimantus want to know a
good justification for the belief that justice is better than
injustice in all cases, even if there is no punishment or it could be
guaranteed that you will never be caught doing unjust acts. In my
opinion, this is a justification that is pretty hard find. We live in
a society where breaking the law is always associated to receive a
punishment. If you don't enquiry a bit more, punishment gives you an
apparent satisfactory answer to this question. However, Plato (through
Socrates) tries to give a deep justification for it.

In books II, III, and IV, Socrates proposes the following strategic to
find a good justification for choosing justice for a good
life. Firstly, they will think about how a society would be just and
then, will do a similar reasoning for an individual person. I think
that there is a fair point for this strategy since as just societies
are desirable and they are constituted by individuals, then it will be
also desirable those individuals provide justice to the society by
being just members of it.

Socrates tries to find the ideal state (or city in Socrates' ages when
there were lots of city-states with different regimens), that is, the
best way of organisation that provides stability to states themselves,
where all citizens can be better off. The proposal divides a state in
the following parts:

* Producers: people providing material goods and services using
  natural or state's resources. Basically, typical jobs as
  shoemakers, teachers, etc.

* Auxiliaries: as the state must be defended against external enemies,
  the auxiliaries will be trained to be the military force.

* Guardians: these would be the rulers, experts on knowledge and
  guiding societies.

In order to be a well-functioning state, Socrates proposes that these
parts must be specialised, so producers, auxiliaries and guardians are
not interchangeable. They must be raised and educated to perform their
job the best they can. It is particularly interesting, for instance,
how Socrates tries to describe the guardians' education (philosophy,
harmony, gymnastic, etc.), social and family live in Book III. For
example, in 416e guardians should not have any private room and
everything must be shared around the guardian community. They are not
allowed to have a salary (only the strictly required for their
subsistence provided by the rest of citizens) nor any personal
material goods or properties unless is completely required.

Although Socrates believes in a strong specialisation of each
political role, "non-capable" guardians can be sent with the rest of
the citizens. Also, a capable producer with innate abilities to be a
guardian could be promoted (423b, book IV). The idea behind this
stratification of the society is that every part contributes to
balance the state itself, so it can be considered as a just society.

Now Socrates focuses the dialogue on the individuals. As it was done
for the state, he finds that each individual's soul has in fact three
parts:

* Appetite: desires, instincts. Similar to the producers part of the
  state.

* Spirit: as in emotions, courage. Similar to the auxiliaries part of
  the state.

* Reason: formal and logic thought. Similar to the guardians part of
  the state.

In a perfect balanced soul, reason dictates and restricts wisely the
actions of the spirit and appetite part. For example, "it is not a
good idea to eat that burger", or "don't yell at your friend, just
calm down". Conflicts between soul's parts can be seen as a civil war
in a state (or, at least, the beginning of). If, for instance, the
spirit (auxiliaries) oppresses the appetites (producers) too much and
the reason (guardians) does not solve the conflict, then the soul
(state) would be in conflict and unstable. Appetite should not take
Spirit's place, as Reason should not eliminate any other. Each part
should do what they are supposed to do, in a perfectly balanced
soul. In other words, a balanced soul is what provides justice to a
person.

So, as it is stated in 444d (Book IV), as healthy things produces
healthy bodies and unhealthy things produces unhealthy bodies, bad
things for the soul produces miserable souls. Injustice can be seen
now as a force to unbalance the individual's soul, a force for making
the different parts to fight each other and, as a result, produces a
tumultuous soul. A life with such soul is not worth to live, even if
that life is full of material things like money or food, because the
soul is what supports everything in the individual's life.

This is the answer to Glaucon and Adeimantus about why justice is
always preferable to injustice: injustice corrupts the soul, and the
soul is what makes our life worth it. The soul is the source of any
good thing we can extract from anything else (friends, money,
goods,... anything). So if you ruin the soul with injustice, you are
ruining everything that comes after that.

There are lots of nice things in The Republic like Plato's Theory of
Forms, the famous Allegory of the Cave, or the interesting theory
about the immortality of the soul at Book X. I will tell you more
about all these parts in future letters.

Best regards,
 Cleto.
