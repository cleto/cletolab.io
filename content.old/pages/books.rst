.. -*- coding: utf-8; mode:rst-mode -*-

:title: Books
:slug: books
:authors: cleto
:date: 12/08/2017
:status: hidden

These are some of the books I have read. I'll try to be as rigorous as
I can. A few notes about this lists:

* I'm not trying to make a thorough description of each book. My
  intention is to try to keep a log just for revisiting it in the
  future.

* I'll use the language on which the book was written to describe
  it.

2019
----

Our Mathematical Universe
~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Max Tegmark
* Category: non-fiction / science
* Score: 4/5
* Comment: a great book I have enjoyed a lot. For the first time I
  have read something related to physics/mathematics that attempts to
  explain our reality as a whole. It's also good for an introduction
  to the most recent physics theories and learning from a great MIT
  professor.

2018
----

El gran libro de la mitología griega
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Robin Hard
* Category: non-fiction / history
* Score: 3/5
* Comment: este es un gran libro sobre los mitos griegos de principio
  a fin y muy detallado. La profundidad con la que trata los mitos se
  puede hacer bastante complicada por el nivel de detalle y
  referencia, pero sin duda ayuda el orden "cronológico" con el que
  aborda todos los mitos.

Teeteto o sobre la ciencia
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: se trata de un diálogo nada sencillo de seguir, debido a
  los análisis lógicos que se hacen de conceptos como el saber o el
  juicio verdarero/falso. Sin embargo, me ha gustado ver cómo aborda
  el intento de definir qué ocurre realmente cuando nos equivocamos al
  emitir juicios erróneos.

El camino
~~~~~~~~~

* Author: Miguel Delibes
* Category: fiction
* Score: 4/5
* Comment: qué agradable lectura y qué bonita es la historia. En
  muchas ocasiones me he sentido parte de la pandilla de Daniel,
  el Mochuelo, porque lo bien que está narrado.


¿Qué es la ciencia?
~~~~~~~~~~~~~~~~~~~

* Author: Gustavo Bueno
* Category: non-fiction / philosophy
* Score: 2/5
* Comment: este es un gran libro que no he debido leer. Me ha servido
  un poco para introducirme en la teoría del cierre categorial, pero
  estoy lejos de poder sacar algún provecho de él. Es realmente
  interesante lo sistemático que es a la hora de definir conceptos y
  proporcionar una teoría de la ciencia. La puntuación es baja por mi
  escaso nivel, no por el libro en sí.

El manifiesto comunista
~~~~~~~~~~~~~~~~~~~~~~~

* Author: Karl Marx & Hegels
* Category: non-fiction / philosophy
* Score: 3/5
* Comment: supongo que tenía muchas ganas a una obra como esta. Tras
  leerlo entiendo un poco mejor el impacto que ha tenido que tener una
  obra como esta en los últimos siglos. Me ha desilusionado un poco
  porque es realmente escueto aunque claramente necesita más
  re-lecturas.

Space merchants
~~~~~~~~~~~~~~~

* Author: Frederik Pohl & Cyril M. Kornbluth
* Category: fiction / sci-fi
* Score: 4/5
* Comment: a great futuristic, dystopia story with lot of very
  philosophical parts that will make you think about our economy and
  society values.


Quantum: a guide for the perplexed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Jim Al-Khalili
* Category: non-fiction / science
* Score: 4/5
* Comment: great introduction about quantum physics, from history to
  the real challenges that it exposes. It might be a bit out-to-date
  but definitively a really well-explained approach to the topic: not
  too deep and not too shallow.

Del sentimiento trágico de la vida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Miguel de Unamuno
* Category: non-fiction / philosophy
* Score: 4/5
* Comment: aunque no me ha resultado fácil de leer, me ha servido para
  entender mucho más claramente la tradición cristiana y
  católica. Sorprenden lo razonable (y también su parte irracional) de
  que son los argumentos para creer en una vida después de la
  muerte y en la religión.

La Regenta
~~~~~~~~~~

* Author: Leopoldo Alas "Clarín"
* Category: fiction / classics
* Score: 5/5
* Comment: es una obra maestra. He de decir que el libro es bastante
  lento al principio y en algunas partes, pero sin duda alguna merece
  la pena. Extraordinariamente culto en sus referencias y recursos
  literarios. Un final apoteósico y muy aleccionador sobre la envidia
  y odio de una sociedad, en el fondo, enferma.

Filebo o del placer, la inteligencia y el bien
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: es un diálogo, en algunas partes, bastante complejo pero
  trata un tema muy interesante con gran profundidad: es mejor una
  vida entregada al placer o a la sabiduría. La respuesta que ofrece
  hoy puede parecer obvia, pero el camino que sigue en su
  justificación es muy interesante.

Fedro o de la belleza
~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: supongo que es por la cantidad de diálogos que llevo leídos
  de hasta el momento, pero tras leerlo me había esperado más de este
  diálogo que es bastante famoso. Muy evocador el sitio donde
  transcurre todo el diálogo y muy buena la ironía que Sócrates a
  veces utiliza.

Lisis o de la amistad
~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: me ha gustado mucho este diálogo porque es bastante
  profundo y sistemático en cuanto a la búsqueda de la definición de
  lo que es ser amigo, o de la amistad en general. Es interesante lo
  contradictorio que puede llegar a ser un término tan usado.

Ion o de la poesía
~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: Sócrates se encuentra con un rapsodista especialista en
  Homero y trata de hacerle ver que ellos son simplemente transmisores
  del arte de la poesía, ya que estos cantan al pueblo lo que los
  poetas escriben. Es curioso que luego Platón tache a la poesía de
  indigna para su estado de la República, porque aquí la pone por las
  nubes en muchos pasajes.

Menéxeno o la oración fúnebre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: un canto a los muertos en combate durante algunas. Está
  curioso porque se describen algunas de las batallas que Grecia tuvo
  y siempre es bueno conocer algo de historia (aunque no estoy seguro
  si es real o méramente mitológica ya que lo describe en tono muy
  patriótico). Me ha gustado aprender sobre la figura de Aspasia, una
  mujer muy reconocida intelectualmente por aquella época.

Hipías Menor o de la mentira
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 2/5
* Comment: pensaba inicialmente que sería un análisis sobre la mentira
  y realmente no es así. Sócrates muestra una serie de paradojas
  durante la discusión sin entrar en el fondo de las cosas.

Hipías Mayor o de lo bello
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: Platón es el filósofo de la belleza y en este diálogo va
  refutando todas las creencias que había sobre este concepto hasta
  sus días. Aunque se puede ver la profundidad en algunos argumentos
  sobre lo que es la belleza, algunas definiciones me parecen muy
  simples. Obviamente, aquí no trata lo bello en su profundidad como
  en otros diálogos.

Protágoras o los sofistas
~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: me parece un gran diálogo sobre cómo Sócrates critica a los
  sofistas, sus métodos y sus valores.

Teages o de la ciencia
~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: para ser educado en las verdaderas ciencias, además de
  tener buenos maestros, es necesario tener la actitud y la
  predisposición adecuadas. Sócrates no se fía de su candidato mucho
  porque parece que no tiene claro qué quiere aprender con él.

2017
----

Laques o del valor
~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: este es un breve diálogo sobre el concepto de valor. Es el
  primero que he leído en el que Sócrates participa en una
  conversación más como árbitro que como guía. Hay mucha polémica en
  algunos momentos y es bastante ameno.

Cármides o de la sabiduría
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: Sócrates habla con un joven que dice ser sabio y saber lo
  que es la sabiduría. Una buena demostración sobre la ingenuidad de
  la juventud. Por cierto, en este diálogo he visto las primeras
  manifestaciones claras sobre `los griegos y la homosexualidad`_

Segundo Alcebíades o de la oración
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 3/5
* Comment: Sócrates advierte de lo peligroso de orar y pedir a los
  dioses ciertas cosas que pueden parecer buenas pero que en el fondo
  son malas si realmente se conceden.


Primer Alcebíades o de la naturaleza humana
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: Sócrates demuestra lo importante que es la sabiduría (o
  virtud, o conocimiento) para la vida pública y personal de un ser
  humano. Me ha gustado mucho el análisis de la frase "conócete a ti
  mismo" del templo de Delfos ("el ojo que se ve en otro ojo para verse
  a sí mismo").

Critón
~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 5/5
* Comment: Sócrates esperando a hacerse efectiva su sentencia de
  muerte hace un gran discurso sobre por qué hay que obedecer las
  leyes y al Estado, incluso en sus propias circunstancias.

Aplogía de Sócrates
~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: en este (casi) monólogo de Sócrates durante su juicio
  expone grandes ideas sobre su deber como filósofo intentando echar
  por tierra los motivos de su acusación. He notado que Sócrates no
  era un gran fan de la religión politeista del momento, aunque nunca
  lo afirma directamente. Otra punto que me ha gustado mucho es la
  defensa de la muerte como un bien y no como un mal.


Eutifrón o de la santidad
~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: breve diálogo entre Sócrates y Eutifrón sobre qué es que
  algo sea santo o impío. Tremendo, por cierto, el análisis de
  Sócrates haciendo caer a Eutifrón en contradicciones en su propia
  definición de lo santo. Escrito hace más de 2400 años, ojo. Es corto
  pero necesita leerlo (y releerlo) con cuidado para digerir algunas
  partes.


Niebla
~~~~~~

* Author: Miguel de Unamuno
* Category: fiction / classics
* Score: 5/5
* Comment: entretenido, gracioso, filosofíco e innovador. Este libro,
  lo tiene todo. Todos estamos en la niebla, entramos y salimos de
  ella continuamente, como Augusto.


Thinking fast and slow
~~~~~~~~~~~~~~~~~~~~~~

* Author: Daniel Kahneman
* Category: non-fiction / psychology
* Score: 2/5
* Comment: (too long) book about how humans make mistakes in
  predictions and sipmle problems. The brain divided in 2 systems (one
  for fast thinking and other for slow) and the concept of "regression
  to the mean" are interesting, but narrative is not appealing at all
  and it was a bit painful to finish.


Justice: what is the right thing to do?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Michael Sanders
* Category: non-fiction / philosophy
* Score: 4/5
* Comment: very nice trip through the different components of
  justice. It also provides a good reasoning about its foundations and
  the challenges that are present nowadays.

What money can't buy: The moral limits of markets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Michael Sanders
* Category: non-fiction / philosophy
* Score: 5/5
* Comment: it changed the way I think of how economic principles try
  to govern our life and what we lose when civic life is replaced by
  market-driven values.


El Aleph
~~~~~~~~

* Author: José Luís Borges
* Category: fiction / philosophy
* Score: 3/5
* Comment: después de leer algún que otro análisis del libro, he de
  decir que es bastante profundo y complejo de entender. Las historias
  hacen bastantes referencias históricas que si no las conoces no le
  sacas todo el jugo.

La República
~~~~~~~~~~~~

* Author: Platón
* Category: fiction / philosophy
* Score: 4/5
* Comment: es sorprendente que sea tan ameno de leer pese a su
  antigüedad y de los conceptos filosóficos de los que habla. Destaco
  de aquí el mito de la caverna, la propuesta del estado ideal y la
  teoría de la transmigración de las almas.

Historia de la filosofía I
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Federick Copleston
* Category: non-fiction / philosophy
* Score: 3/5
* Comment: he aprendido bastante sobre la filosofía antigua (griega y
  romana). El libro no es fácil de leer pese a que estaba dirigido a
  estudiantes (mucho más listos que yo, por lo que se ve). Merece la
  pena el esfuerzo porque da una visión muy detallada y estructurada,
  aunque he echado en falta algunas anécdotas o ejemplos que hagan más
  entretenida la lectura.

2016
----

De animales a dioses
~~~~~~~~~~~~~~~~~~~~

* Author: Yuval Noah Harari
* Category: non-fiction / history
* Score: 4/5
* Comment: me ha gustado la forma en que re-interpreta la historia del
  hombre, como especie, en un tono más científico que
  político. Gracias a mi amigo Raúl por este libro.

Don Quijote de la Mancha
~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Miguel de Cervantes Saavedra
* Category: fiction / classic
* Score: 5/5
* Comment: con razón tiene la fama que tiene. Me lo he pasado genial
  con este libro pese a que se puede hacer duro en algunas
  partes. Cervantes hace una demostración de cultura y sabiduría
  brutal.


About time: Eistein's unfinished revolution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Paul Davis
* Category: non-fiction / science
* Score: 4/5
* Comment: if you are interested in time, this is a great book to
  start for a scientific point of view. It reviews many different
  aspects of time, historical review and experiments. Also, good
  presentation of Eistein's theory of relativity.


The moral foundations of politics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Ian Shapiro
* Category: non-fiction / philosophy
* Score: 3/5
* Comment: it is a good historical review of philosophy of politics,
  how societies organise and how they agree on a certain set of
  principles (utilitarianism, liberalism, Marxism, social contract,
  etc.) I read this book as part of an online course and that helped
  to understand it better. I finished the book expecting more answers
  and I guess political philosophy is not what I thought it would be.

2015
----

Metaphysics
~~~~~~~~~~~

* Author: Peter van Inwagen
* Category: non-fiction / philosophy
* Score: 4/5
* Comment: the very short introduction was too short for me, so I read
  this book about the same topic. Really great book explaining key
  concepts like the ontological and cosmological arguments, time,
  being, free will, etc. Do not expect answers but great food for
  thought.


Metaphysics: a very short introduction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Stephen Mumford
* Category: non-fiction / philosophy
* Score: 4/5
* Comment: one of the famous Oxford book series, it is a great simply
  introduction to metaphysics, a field that I was interested to
  explore after reading "Lecciones preliminares de filosofía".


Lecciones preliminares de filosofía
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Manuel García Morente
* Category: non-fiction / philosophy
* Score: 5/5
* Comment: este fue mi primer libro de filosofía tras haberme visto la
  serie de vídeos online "Filosofía aquí y ahora". Sin duda alguna,
  una gran introducción a la filosofía muy accesible. Son clases
  magistrales en las que se repasa historia y definiciones básicas de
  filosofía, todas ellas relacionadas entre sí y con un gran hilo
  conductor.


¿Qué es el universo? ¿Qué es el hombre?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Eduardo Bataner
* Category: non-fiction / science
* Score: 4/5
* Comment: es una muy buena exposición sobre cosmología (historia y
  retos actuales), bastante fácil de digerir (obviamente necesita un
  poco de formación científica previa), donde el ser humano tiene una
  especial mención (algunas de ellas especialmente filosóficas y
  existenciales). Gracias a mi amigo Eduardo por este libro.

2014
----

Elemental design patterns
~~~~~~~~~~~~~~~~~~~~~~~~~

* Author: Jason McC. Smith
* Category: non-fiction / computing
* Score: 4/5
* Comment: very attractive attempt to give solid and structured
  foundations to design patterns. It proposes elemental bits of
  designs (as they were atoms) on which all design patterns can be
  built of. Also, an interesting way of representing diagrams of
  design patterns in a collapsible way (called PIN) is described. It
  is an inspiring book since it shows that combining all these atomic
  design pieces systematically you could obtain the very basic pieces
  of a program design.

Other books
-----------

This part of the list is a bit more chaotic. I know I have read these
books but either I don't remember when or can't say what they were
about. The format of this list is simple: ``title / author`` sorted
alphabetically. I'll be moving elements of this list to other years if
I re-read them or remember more details about them.

* Contacto / Carl Sagan
* El cerebro de Broca / Carl Sagan
* El hacedor de estrellas / Olaf Stapledon
* El valle de los leones / Kent Follent
* Los pilares de la tierra / Kent Follent
* Ubik / Philip K. Dick
* El hombre ilustrado / Ray Bradbury
* Crónicas marcianas / Ray Bradbury
* Fahrenheit 451 / Ray Bradbury


.. _los griegos y la homosexualidad: https://es.wikipedia.org/wiki/Homosexualidad_en_la_Antigua_Grecia

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:
