.. -*- coding: utf-8; mode:rst-mode -*-

:title: About me
:slug: about
:authors: cleto
:date: 23/07/2016
:modified: 12/08/2017

Hey! This is Cleto. I'm a software engineer basically interested in
GNU/Linux and Free Software. Whenever I am not at work or with my
family, I try to do `some coding <https://gitlab.com/cleto>`_, work
on Debian GNU/Linux, read, and go for a run.

Contact
=======

If you want to contact me, you can do so via:

* Email: `blog` *at* `cleto` *dot* `es`

* IRC: `cleto` on `irc.freenode.net`.

* On `LinkedIn <https://www.linkedin.com/in/cleto>`_.

* On `telegram <https://telegram.me/cleto>`_.

* On `twitter <https://twitter.com/cletomartin>`_, but I don't really
  pay attention to it.

You will **not** find me on Facebook nor WhatsApp.

Pesonal stuff
=============

These are some personal things that you might find interesting to have
a look:

* `Books </pages/books.html>`_ I have read.

..
   Local Variables:
     mode: flyspell
     ispell-local-dictionary: "british"
   End:
