.. -*- mode:rst; mode:flyspell; coding:utf-8 -*-

.. include:: global.links

Entradas antiguas en vigilando.org
##################################

:date: 2017/01/29
:tags: vigilando
:category: fun
:slug: entradas-antiguas-vigilando
:author: Cleto Martin
:lang: es
:summary: He encontrado algunas capturas de la antigua vigilando.org
   en `archive.org`_ y aquí tengo algunas de mis entradas que
   me gustaría guardar para la posteridad.

He de decir que me he reído bastante leyéndo algunas entradas que he
encontrado en el `apartado de vigilando.org en archive.org`_. No por
el contenido en sí (que daría para un análisis delirante), sino por
recordar como un grupo de amigos montó un pequeño portal para
básicamente publicar bromas y anécdotas.

¡Y cómo hemos cambiado! No sólo los integrantes de ese grupo de
amigos, sino la sociedad y el uso de la tecnología. Hay posts
simplemente compartiendo un vídeo que al autor le hacía gracia, y el
resto aportaba su comentario. Nada que ver a cómo las redes sociales
funcionan hoy. Hoy eso mismo se despacha con un tweet y llega a
personas que jamás verás en tu vida a base de re-tweets.

Los siguientas artículos los he copiado y pegado aquí para la
posteridad. Son posts en los que detallo algunos extraños sucesos que
realmente me ocurrieron, contados con algo de broma pero son 100%
verídicos.

Solo me pasa a mí
-----------------

Entrada publicada publicada a las 05:48h, el 21 Septiembre de 2008::

  ¡Hola amigos! Os voy a contar una historia que me acaba de pasar,
  totalmente surrealista. Son las 6:30 de la mañana y, de verdad, lo
  que no me pase a mí...

  El caso es que vengo de ver una película en casa de Kitty. La peli
  en cuestión ha estado bien, se titula "New York New York" donde
  podemos ver a Robert de Niro bien joven. Una parte de la peli es
  musical (como unos 20 minutos) y hacia el final. A mí me dan
  angustia los musicales y me ha entrado un poco la mala leche. Menos
  mal que todo no ha sido un musical.

  El caso es que, ya cansado y tal, me voy para afuera y me despido de
  mi señora. Me pongo el casco, arranco la moto y como Lorenzo Lomeras
  que soy, voy como una centella hacia mi casa. Como eran las 6 y
  pico, pues me meto en dirección prohibida por la calle Carlos Eraña
  que no pasa ni dios a esa hora. Lo he hecho varias veces.

  Tuerzo en una calle hacia la izquierda y subo en dirección al parque
  nuevo este que han hecho. Ese que es muy moderno, donde está el
  videoclub. Haciendo un STOP me encuentro que en la tienda que hace
  esquina (un concesionario de coches) hay un monitor de 40 pulgadas
  con un pantallazo azul de la muerte (o sea, un pantallazo de
  Windows). Y digo: "Joer, que pena que no tengo el móvil y la subía a
  Vigilando".

  Me quedo mirando el pantallazo y vuelvo a ponerme en marcha hacia mi
  casa. Hacia la mitad de la calle viene una persona y se pone en
  medio. No de repente, sino a lo lejos. Digo: "Ya estamos...".

  Al acercarme veo que se trata de una sudamericana. Me dice algo,
  pero no la oigo. Me quito el casco y le pido que me repita lo que me
  ha dicho. Y me dice: "Ay mi amol, ¿dónde está la estación de
  autobusel?". Digo: "Hombre, si está aquí al lado. A un minuto. Cruza
  este parque...."

  Me corta: "Ay... llevo 3 horas andando...". Me bajo de la moto y al
  acercarme huelo a alcohol. Yo intento insistir: "No te preocupes, si
  está aquí al lado. Mira, cruza este parque y estás ya en na.... en 1
  minuto. Si se ve desde aquí". A lo que ella apostilla: "Ay mi amol,
  ¿no me puedes lleval?". Totalmente cagado respondo: "Pero si está
  aquí al lado....". Y me dice con más tristeza: "Ay llevamel, por
  favol, llevo 3 horas andando...".

  Para mi sorpresa, digo: "Venga, sube". En cuestión de 10 segundos,
  me encuentro en una situación totalmente surrealista. Llevando en mi
  moto como paquete a una sudamericana beoda después de haber visto un
  pantallazo azul, sin olvidar el musical de De Niro.

  El camino con la "amiga" ha sido de las peores cosas que he pasado
  psicológicamente. Imaginad: llevas a alguien detrás, que no conoces
  de nada, borracha y agarrado levemente a tu cintura. Ha sido una
  situación extrañísima. He pensado que podía sacar una navaja de su
  bolsillo y clavármela, pero perfectamente y sin problema. Por mucho
  tío que yo fuera, estaba totalmente vulnerable. Por eso, he ido
  acelerando.

  Llegamos a una esquina cerca de la estación y yo no veo allí a
  nadie. Ella decía que salía un autobús para Daimiel, pero
  vamos... en la estación no estaba ni "Panete". De todas formas, al
  bajarse la he mirado a los ojos y le he dicho: "Bueno, ahí está. Nos
  vemos". Y ella me ha dicho algo que siempre se me quedará grabado:
  "Gracias. Ve con Dios". Y ha salido corriendo hacia la estación.

  Y hállome aquí, amigos míos, en una situación de que si no lo cuento
  no puedo dormir. Puede ser que os parezca una tontería pero, en fin,
  a mí me ha marcado.

  Un saludo!


La gitana lesbiana
------------------

Entrada publicada publicada a las 07:34h, el 15 Febrero de 2009::

  ¡Hola amigos! Acabo de llegar a mi casa, son las 7:00 de la mañana, y
  os escribo para contaros otra curiosa historia que me acaba de pasar.

  Enfrascado en mis pensamientos, conducía la moto hasta mi casa. Como
  siempre, llego al semáforo de la ronda de Ciruela (en frente del
  "AmesaMerican" Grill) y, como siempre, está en rojo. Me paro pese a
  que no viene ni Cristo y empiezo a esperar a que se ponga en verde.

  Situado en el carril izquierdo, miro por el retrovisor y viene un
  coche a toda pastilla por el carril de la derecha. Iba bastante
  rápido, por lo que supuse que sería algún "mortá" que viene de una
  fiesta chunga y le flipa ir con las lunas "tintás" y el alerón rosa.

  El cabrito del coche no paraba. Le quedaban pocos metros para llegar
  al semáforo y parecía que iba más rápido. "Me la va a liar" - pienso
  mientras desembrago y meto marcha a la moto por si hay que salir
  corriendo (mucho mejor que salir volando). Cuando ya le queda poco (y
  yo ya me estaba apartando ligeramente) pega el megafrenazo y se para
  justo en la raya.

  Pese a que yo soy consciente de que un "mortá" es muy tonto, sabía
  perfectamente que no era un "mortá"; porque para hacer eso hay que
  haber visto mucho mundo y estar de vuelta de todo. Miro las llantas y
  parecen buenas de cojones. No me quedaba la menor duda: era un gitano
  el que conducía.

  Sin saber muy bien por qué, sigo subiendo la mirada hasta que me
  encuentro con el careto del gitano mirándome con una cara de
  odio/drogado que me ha dejado helado. Protegida la mirada con el
  casco, paso rápidamente a mirar al semáforo como diciendo: "Voy a ver
  si se pone en verde ya...". Mientras veía por el rabillo del ojo al
  gitano aún mirándome.

  Unos segundos de tensión más: yo mirando al semáforo y el gitano
  mirándome aún. De repente, veo que deja de mirarme y se gira hacia el
  interior del coche y se oye una voz de mujer (gitana):

  - "AYYYY, no me toooooques las teeeetas, Ramón!!!!".

  No daba crédito. Sube un poco la música y se oye de nuevo a aquella
  mujer:

  - "Ramón, cooooño, que sabes que soy tortillera. Tócaselas a oootra,
    joooder!"

  Y el semáforo se pone en verde. Aún tenía la marcha puesta, no me
  había dado ni cuenta. Pero no salí el primero, preferí que "me ganara"
  y se marchara.

  Cuando sale, como buen "makoki", quema ruedas y yo salgo detrás de él
  más despacito. En vez de irme a la glorieta del "Anti"Quijote Azteca,
  hago un giro prohibido hacia la izquierda para evitar al personaje. Y
  menos mal que he hecho ese giro. Justo cuando lo hago, sale un mini
  volando de la ventanilla de atrás del coche en dirección a donde yo
  podría haber estado de seguir las normas de tráfico.

  ¿Moraleja? Si sigues las normas de tráfico, los ocupantes de un coche
  conducido por un gitano (con una gitana lesbiana dentro) te tirarán un
  mini.

..
   Local Variables:
     ispell-local-dictionary: "british"
   End:
